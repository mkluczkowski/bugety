// Budget Controller
var budgetController = (function()
{

    var Expense = function(id, description, value)
    {
        this.id = id;
        this.description = description;
        this.value = value;
        this.percentage  = -1
    };

    Expense.prototype.calcPercentage = function(totalIncome)
    {
        if (totalIncome > 0) 
        {
            this.percentage = Math.round((this.value / totalIncome) * 100); 
        }
        else
        {
            this.percentage = -1;
        }
    };

    Expense.prototype.getPercentage = function()
    {
        return this.percentage;
    };

    var Income = function(id, description, value)
    {
        this.id = id;
        this.description = description;
        this.value = value;
    };

    var calculateTotal = function(type)
    {
        var sum = 0;

        data.allItems[type].forEach(function(current)
        {
            sum = sum + current.value;
        });

        data.totals[type] = sum;
    }

    var data = {
        allItems: {
            exp: [],
            inc: []
        },
        totals: {
            exp: 0,
            inc: 0
        },
        budget: 0,
        percentage: -1
    }

    return {
        addItem: function(type, des, val)
        {
            var newItem, ID;

            // Create new ID
            if(data.allItems[type].length > 0)
            {
                ID = data.allItems[type][data.allItems[type].length - 1].id + 1;
            }
            else
            {
                ID = 0;
            }

            // Create new Item
            if (type == 'exp') 
            {
                newItem = new Expense(ID, des, val);   
            }
            else if(type == 'inc')
            {
                newItem = new Income(ID, des, val);
            }

            // Push into array
            data.allItems[type].push(newItem);

            // Return new item
            return newItem;
        },

        deleteItem: function(type, id)
        {
            var ids, index;
            
            ids = data.allItems[type].map(function(current)
            {
                return current.id;
            });

            index = ids.indexOf(id);

            if (index != -1) 
            {
                data.allItems[type].splice(index, 1);    
            }
        },

        calculateBudget: function()
        {
            // Calculate total income and expenses
            calculateTotal('exp');
            calculateTotal('inc');

            // Claculate the budget: inc - exp
            data.budget = data.totals.inc - data.totals.exp;

            // Claculate %
            if (data.totals.inc > 0) {
                data.percentage = Math.round((data.totals.exp / data.totals.inc) * 100);
            }
            else
            {
                data.percentage = -1;
            }

        },

        calculatePercentages: function()
        {
            data.allItems.exp.forEach(function(currentElement)
            {
                currentElement.calcPercentage(data.totals.inc);
            })
        },

        getPercentages: function()
        {
            var allPerc = data.allItems.exp.map(function(currentElement)
            {
                return currentElement.getPercentage();
            })

            return allPerc;
        },

        getBudget: function()
        {
            return{
                budget: data.budget,
                totalInc: data.totals.inc,
                totalExp: data.totals.exp,
                percentage: data.percentage
            }
        },

        testing: function()
        {
            console.log(data);
        }
    }
    
})();

// UI Controller
var UIController = (function()
{
    var DOMstrings = {
        inputType: '.add__type',
        inputDesc: '.add__description',
        inputValue: '.add__value',
        inputBtn: '.add__btn',
        incomeContainer: '.income__list',
        expensesContainer: '.expenses__list',
        budgetLabel: '.budget__value',
        incomeLabel: '.budget__income--value',
        expenseLabel: '.budget__expenses--value',
        percentageLabel: '.budget__expenses--percentage',
        container: '.container',
        expensesPercLabel: '.item__percentage',
        dateLable: '.budget__title--month'
    };

    var formatNumber = function(number, type)
    {
        var numSplit, int, dec, type;

        /* 
        
        + or - before number
        2 decimals
        comma separating thousends

        2310.4567 -> + 2,310.46
        
        */

        number = Math.abs(number);
        number = number.toFixed(2);

        numSplit = number.split('.');

        int = numSplit[0];        

        if (int.length > 3) 
        {
            int = int.substr( 0, int.length - 3 ) + ',' + int.substr( int.length - 3, 3 );
        }

        dec = numSplit[1];

        return (type == 'exp' ? sign = '-' : sign = '+') + ' ' + int + '.' + dec;
    };

    var nodeListForEach = function(list, callback)
    {
        for (var i = 0; i < list.length; i++) 
        {
            callback(list[i], i);
        }
    }

    return {
        getInput: function()
        {
            return {
                type: document.querySelector(DOMstrings.inputType).value, // inc or exp 
                description: document.querySelector(DOMstrings.inputDesc).value,
                value: parseFloat(document.querySelector(DOMstrings.inputValue).value),
            };
        },

        addListItem: function(obj, type)
        {
            var html, newHtml, element;
            // 1. Create HTML string with placeholder
            if (type == 'inc') 
            {
                element = DOMstrings.incomeContainer;
                html = '<div class="item clearfix" id="inc-%id%"><div class="item__description">%description%</div> <div class="right clearfix"> <div class="item__value">%value%</div><div class="item__delete"><button class="item__delete--btn"><i class="ion-ios-close-outline"></i></button></div></div></div>'   
            }
            else if(type == 'exp')
            {
                element = DOMstrings.expensesContainer;
                html = '<div class="item clearfix" id="exp-%id%"><div class="item__description">%description%</div><div class="right clearfix"><div class="item__value">%value%</div><div class="item__percentage">21%</div><div class="item__delete"><button class="item__delete--btn"><i class="ion-ios-close-outline"></i></button></div></div></div>'
            }
            
            // 2. Replace placeholder with data
            newHtml = html.replace('%id%', obj.id);
            newHtml = newHtml.replace('%description%', obj.description);
            newHtml = newHtml.replace('%value%', formatNumber(obj.value, type));

            // 3. Insert into DOM
            document.querySelector(element).insertAdjacentHTML('beforeend', newHtml);
        },

        deleteListItem: function(selectorId)
        {
            var el = document.getElementById(selectorId);

            el.parentNode.removeChild(el);
        },

        clearField: function()
        {
            var fields, fieldsArray;

            fields = document.querySelectorAll(DOMstrings.inputDesc + ', ' + DOMstrings.inputValue);

            fieldsArray = Array.prototype.slice.call(fields);

            fieldsArray.forEach(function(current, index, array)
            {
                current.value = "";
            });

            fieldsArray[0].focus();
        },

        displayBudget: function(obj)
        {
            obj.budget > 0 ? type = 'inc' : type = 'exp'

            document.querySelector(DOMstrings.budgetLabel).textContent = formatNumber(obj.budget, type);
            document.querySelector(DOMstrings.incomeLabel).textContent = formatNumber(obj.totalInc, 'inc');
            document.querySelector(DOMstrings.expenseLabel).textContent = formatNumber(obj.totalExp, 'exp');
        
            if (obj.percentage > 0) 
            {
                document.querySelector(DOMstrings.percentageLabel).textContent =  obj.percentage + '%'; 
            }
            else
            {
                document.querySelector(DOMstrings.percentageLabel).textContent = '---';
            }
        },

        displayPercentages: function(percentages)
        {
            var fields = document.querySelectorAll(DOMstrings.expensesPercLabel);

            nodeListForEach(fields, function (current, index) 
            {
                if (percentages[index] > 0) 
                {
                    current.textContent = percentages[index] + '%'; 
                }
                else
                {
                    current.textContent = '---'  
                }
                  
            })

        },

        displayMonth: function()
        {
            var now, year, month, months;

            now = new Date();
            
            year = now.getFullYear();

            month = now.getMonth();

            months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

            document.querySelector(DOMstrings.dateLable).textContent = months[month] + ' ' +  year;
        },

        changedType: function()
        {
            var fields = document.querySelectorAll(
                DOMstrings.inputType + ',' +
                DOMstrings.inputDesc + ',' + 
                DOMstrings.inputValue
            );   

            nodeListForEach(fields, function(currentElement)
            {
                currentElement.classList.toggle('red-focus');
            });

            document.querySelector(DOMstrings.inputBtn).classList.toggle('red')
        },

        getDOMstrings: function()
        {
            return DOMstrings;
        }
    };
    
})();

// Global App Controller
var controller = (function(budgetCtrl, UICtrl)
{

    var setupEventListners = function()
    {
        var DOM = UICtrl.getDOMstrings();

        document.querySelector(DOM.inputBtn).addEventListener('click', ctrlAddItem);

        document.addEventListener('keypress', function(event)
        {
            if (event.keyCode == 13 || event.which == 13) 
            {
                ctrlAddItem();
            }
        });

        document.querySelector(DOM.container).addEventListener('click', ctrlDeleteItem);

        document.querySelector(DOM.inputType).addEventListener('change', UICtrl.changedType);
    };

    var updatePercentages = function()
    {

        // 1. Calculate percentages
        budgetCtrl.calculatePercentages();

        // 2. Read percentages from budget controller
        var precentages = budgetCtrl.getPercentages();

        // 3. Update UI with the new percentage
        UICtrl.displayPercentages(precentages)

    };

    var updateBudget = function()
    {
        // 1. Calculate budget
        budgetCtrl.calculateBudget();

        // 2. Return budget
        var budget = budgetCtrl.getBudget();

        // 3. Display budget
        UICtrl.displayBudget(budget);

    };

    var ctrlAddItem = function()
    {
        var input, newItem;
        
        // 1. Get input value
        input = UICtrl.getInput();
        
        if (input.description != "" && !isNaN(input.value) && input.value > 0) 
        {
            // 2. Add item to budgetCtrl
            newItem = budgetCtrl.addItem(input.type, input.description, input.value);

            // 3. Add item to UI
            UICtrl.addListItem(newItem, input.type);

            // 4. Clear fields
            UICtrl.clearField();

            // 5. Calculate and update budget
            updateBudget();   

            // 6. Calculate and update percentages
            updatePercentages();
        }
    };

    var ctrlDeleteItem = function(event)
    {
        var itemId, splitId, type, Id;

        itemId = event.target.parentNode.parentNode.parentNode.parentNode.id;

        if (itemId) 
        {
            splitId = itemId.split('-');
            type = splitId[0];
            Id = parseInt(splitId[1]);

            // 1. Delete item from data structure
            budgetCtrl.deleteItem(type, Id);

            // 2. Delete item from UI
            UICtrl.deleteListItem(itemId);

            // 3. Update and show budget
            updateBudget();

            // 4. Calculate and update percentages
            updatePercentages();
        }
        
    };

    return {
        init: function()
        {
            UICtrl.displayMonth();
            UICtrl.displayBudget({
                budget: 0,
                totalInc: 0,
                totalExp: 0,
                percentage: -1});
            setupEventListners();
        }
    };

})(budgetController, UIController);

controller.init();